/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//alert('hello, world');

$(document).ready(function () {
    //var element = document.getElementById('tester');
    var linkClicked = false;
    var element = $('#tester');

    $(element).mouseover(function () {
        //element.fadeOut(500).fadeIn(500);
        $(this).fadeOut(500).fadeIn(500);

    });
    
    $(window).click(function(){
        //this should hide the extended submenu of the top menu regardless of where the user clicked
        $('#menu > ul > li').each(function(){
            if($(this).hasClass("extended")){
                $(this).children('ul').toggle(250);
                $(this).removeClass("extended");
            }
        });
    });
    
    var switchId = function () {
        var rightColumn = $('#right-column');
        var leftColumn = $('#left-column');
        if (!linkClicked) {
            rightColumn.css('left', '0px');
            leftColumn.css('left', '660px');
        } else {
            rightColumn.css('left', '320px');
            leftColumn.css('left', '0px');
        }
    };
    var switchColumns = function () {
        if (!linkClicked) {
            $('.column').css('float', 'right');
        } else {
            $('.column').css('float', 'left');
        }
    };

    $('#right-column a').click(function () {
        //alert('anchor click');
        switchColumns();
        linkClicked = !linkClicked;

        //$('#right-column').css('float','right');
    });

    $('#slidingBox .label').click(function () {
        if ($('#slidingBox').hasClass('slide-out')) {
            $('#slidingBox').removeClass('slide-out');
        } else {
            $('#slidingBox').addClass('slide-out');
        }
    });

    $('#slidingBox a').click(function (e) {

        if ($(this).hasClass('clicked')) {
            $(this).text('Zapytaj jeszcze raz').removeClass('clicked');
        } else {
            $(this).text('Na pewno?').addClass('clicked');
            e.preventDefault();
        }
    });

//    $('.contactForm form').submit(function (e) {
//        alert('hello');
//        e.preventDefault();
//
//
//        $(this).children().find('textarea, input').each(function () {
//            if ($(this).attr('type') !== 'submit') {
//                if ($(this).val() === '') {
//                    alert('Nie wypełniłeś pola ' + $(this).attr('placeholder'));
//                    $(this).addClass('error');
//                } else {
//                    $(this).addClass('valid');
//                }
//            }
//        });


//    });

    $('#menu a').click(function (e) {
        e.stopPropagation();
         $(this).parent().siblings().each(function(){
            if($(this).hasClass("extended")){
                $(this).children('ul').toggle(250);
                $(this).removeClass("extended");
            }
        });
        var neighbour = $(this).next();
        if (neighbour.is("ul")) {
            e.preventDefault();
            neighbour.toggle(250);
            neighbour.parent().addClass("extended");
        }

    });
    
    $('.tabs li a').click(function (e) {
//        e.preventDefault();
//        var tab_id = $(this).attr('data-rel');
//        $(this).parent().siblings().each(function(){
//            if($(this).hasClass('active')){
//                $(this).removeClass('active');
//                $(this).children().each(function(){
//                    $(this).removeClass('active');
//                })
//            }
//        });
//        
//        $(this).parent().addClass('active');
//        $(this).addClass('active');
//        $('div.tab').each(function(){
//            if($(this).attr('id') === tab_id && $(this).hasClass('active')){
//                alert('hello');
//            }
//            else if($(this).attr('id') !== tab_id 
//                    && $(this).hasClass('active')){
//                $(this).removeClass('active');
//            }
//            else if($(this).attr('id') === tab_id && !$(this).hasClass('active')){
//                $(this).addClass('active');
//            }
//        });
        $(this).parents('ul').children().removeClass('active');
        $(this).parent().addClass('active');

        var rel = $(this).attr('data-rel');

        $('div.tab').each(function () {
            if ($(this).attr('id') === rel) {
                //$(this).show();
                $(this).addClass('active');
            } else {
                //$(this).hide();
                $(this).removeClass('active');
            }
        });

    });

    $(document).mousemove(function (e) {
        //alert('cursor');
        $('p#output').text("The coursor is at the x: " + +e.pageX + " and y = " + e.pageY);
    });


    var owl = $("#owl-demo");

    owl.owlCarousel({
        navigation: true,
        singleItem: true,
        transitionStyle: "backSlide"
    });

    $('.contactForm form').validate({
        rules: {
            "contact[subject]": {required: true, minlenght: 3},
            "contact[name]": {required: true, minlenght: 3},
            "contact[email]": {required: true, email: true},
            "contact[content]": {required: true, minlength: 20}
        },
        messages: {
            "contact[subject]": "brak tematu",
            "contact[name]": "brak nazwy",
            "contact[email]": "brak adresu",
            "contact[content]": "brak treści"
        }
    });


   $("#owl-example").owlCarousel();
});
