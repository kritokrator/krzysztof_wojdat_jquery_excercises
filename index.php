<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>JQuery</title>
        <!--<link rel="stylesheet" type="text/css" href="css/style.css"/>-->
        <link rel="stylesheet" type="text/css" href="css/sassStyles.css"/> 

        <link rel="stylesheet" type="text/css" href="gallery-plugin/owl-carousel/owl.carousel.css"/>
        <link rel="stylesheet" type="text/css" href="gallery-plugin/owl-carousel/owl.theme.css"/>
        <link rel="stylesheet" type="text/css" href="gallery-plugin/owl-carousel/owl.transitions.css"/>


        <script type="text/javascript" src="js/jquery-3.1.0.js"></script>


        <script type="text/javascript" src="gallery-plugin/owl-carousel/owl.carousel.js"></script>
        <script type="text/javascript" src="js/jquery.validate.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>

    </head>
    <body>
        <div id="wrapper">

            <div id="header" class="container" data-rel="smth">
                <div id="menu">
                    <ul>
                        <li>
                            <a href="https://www.google.com">Element_1</a>
                            <ul>
                                <li><a href="https://www.google.com">Element_1</a></li>
                                <li><a href="https://www.google.com">Element_2</a></li>
                            </ul>
                        </li>
                        <li><a href="https://www.google.com">Element_2</a></li>
                        <li>
                            <a href="https://www.google.com">Element_3</a>
                            <ul>
                                <li><a href="https://www.google.com">Element_1</a></li>
                                <li><a href="https://www.google.com">DUPA DUPA DUPA</a></li>
                            </ul>
                        </li>
                        <li><a href="https://www.google.com">Element_4</a></li>
                    </ul>
                </div>
            </div>
            <div id="content" class="container">
                <h1 class="title">Tytuł</h1>
                <div id="left-column" class="column">
                    Lewa kolumna
                    <ul class="tabs">
                        <li class="active"><a href="#" class="active" data-rel="tab_1">tab_1</a></li>
                        <li><a href="#"  data-rel="tab_2">tab_2</a></li>
                        <li><a href="#" data-rel="tab_3">tab_3</a></li>
                        <li><a href="#" data-rel="tab_4">tab_4</a></li>
                        <li><a href="#" data-rel="tab_5">tab_5</a></li>
                    </ul>
                    <div id="tab_1" class="tab active myTab">TAB1</div>
                    <div id="tab_2" class="tab">TAB2</div>
                    <div id="tab_3" class="tab okTab">TAB3</div>
                    <div id="tab_4" class="tab redTab">TAB4</div>

                </div>
                <div id="right-column" class="column">
                    Prawa kolumna
                    <a href="#" onclick="return false">click me</a>
                    <div id="coursorDisplay">
                        <p id="output">The coursor is at the x: 0 and y = 0</p>
                    </div>
                </div>
            </div>
            <div id="gallery" class="container">
                <div id="owl-demo" class="owl-carousel owl-theme">
                    <div class="item"><img src="gallery-plugin/demos/assets/fullimage4.jpg"></div>
                    <div class="item"><img src="gallery-plugin/demos/assets/fullimage5.jpg"></div>
                    <div class="item"><img src="gallery-plugin/demos/assets/fullimage6.jpg"></div>

                    <div class="item"><img src="gallery-plugin/demos/assets/fullimage1.jpg"></div>
                    <div class="item"><img src="gallery-plugin/demos/assets/fullimage2.jpg"></div>
                    <div class="item"><img src="gallery-plugin/demos/assets/fullimage3.jpg"></div>

                </div>

            </div>

            <div id="footer" class="container">
                <div id="corporate" class="table-gallery">
                    <div class="gallery-item">
                        <a href="#"><img src="img/logos/l1.jpg" /></a>
                    </div>
                    <div class="gallery-item">
                        <a href="#"><img src="img/logos/l2.png" /></a>
                    </div>
                    <div class="gallery-item">
                        <a href="#"><img src="img/logos/l3.png" /></a>
                    </div>
                    <div class="gallery-item">
                        <a href="#"><img src="img/logos/l4.png" /></a>
                    </div>
                    <div class="gallery-item">
                        <a href="#"><img src="img/logos/l5.jpg" /></a>
                    </div>
                </div>
                 <div id="owl-example" class="owl-carousel">
                    <div class="item">
                        <a href="#"><img src="img/logos/l1.jpg" /></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="img/logos/l2.png" /></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="img/logos/l3.png" /></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="img/logos/l4.png" /></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="img/logos/l5.jpg" /></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="img/logos/l6.png" /></a>
                    </div>
                    ...
                </div>
                <div class="contactForm">

                    <form method="POST">

                        <div class="row">
                            <label for="subject">Temat:</label>
                            <input placeholder="Temat" id="subject" type="text" name="contact[subject]" class="text" value="" />
                        </div>

                        <div class="row">
                            <label for="name">Imię i Nazwisko:</label>
                            <input placeholder="Imię i Nazwisko" id="name" type="text" name="contact[name]" class="text" value="" />
                        </div>

                        <div class="row">
                            <label for="email">Email:</label>
                            <input placeholder="Email" id="email" type="text" name="contact[email]" class="text" value="" />
                        </div>

                        <div class="row">
                            <label for="contactContent">Treść:</label>
                            <textarea id="contactContent" name="contact[content]" class="text" >Treść</textarea>
                        </div>

                        <div class="row submit">
                            <input id type="submit" class="button" value="Wyślij" />
                        </div>

                    </form>

                </div>

            </div>
        </div>
        <div id="slidingBox">
            <div class="label">
                E 
            </div>
            <div class="content">
                <a href="#" >Zawartosc</a>
            </div>
        </div>

    </body>
</html>
